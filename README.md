# Zira de la Zenika Academy

## Consignes et conseils

**Lisez intégralement le document avant de démarrer.**

Pour cette évaluation, on vous demande de développer une application de zéro.

Le cahier des charges est découpé en différents « Chantiers » regroupant plusieurs fonctionnalités qui partagent un même « thème ».

**Il n'est pas nécessaire de finir toutes les fonctionnalités d'un chantier avant de passer au suivant**, vous êtes libres de choisir l'ordre dans lequel vous abordez ces différents chantiers.

### Livrable

Un repo git contenant le front et le back (ainsi que les pipelines, dockerfiles...) ou deux repos git : un pour le front et un pour le back.
Votre projet peut être créé dans votre espace personnel gitlab ou dans le groupe de la Zenika Academy. Envoyez votre repo (ou vos repos) sur slack **dès le début de l'évaluation** en message privé aux membres du jury dont les noms vous seront communiqués.

L'historique git sera valorisé : on essaie de faire des commits ayant du sens et des branches pour chaque chantier ou chaque tâche.

Les tests automatisés seront valorisés également s'ils existent et qu'ils passent :)

### Barême

Répartition approximative de la notation :

- 50% sur le backend (Java, Spring, Postgres, JPA...) ;
- 30% pour l'application front (Javascript, Angular) ;
- 20% pour la pipeline, les dockerfiles, les tests ...

Il n'y a pas de barème précis pour chaque tâche. Le cahier des charges est volontairement trop long pour la durée de l'évaluation, ce qui vous permet (en partie) de choisir sur quelles fonctionnalités travailler.

**Le critère esthétique n'a pas d'importance**. On préfère une interface qui fonctionne et on ne demande pas nécessairement une interface « belle ».

La qualité de votre présentation aura forcément un impact sur la notation, car elle mettra en valeur les éléments que vous choisirez de montrer (nous n'aurons pas le temps d'inspecter tout votre code en détail...).

### Conseils

* Lisez tout le cahier des charges avant de démarrer.
* Le cahier des charges est volontairement trop long. Pas d'inquiétude si vous n'en codez qu'une partie.
* **Demandez de l'aide**. Nous ne sommes pas intéressés par votre capacité à mémoriser des noms de méthodes ou des astuces partagées rapidement il y a trois semaines... Demandez de l'aide autant que lors des TPs hors évaluation. Le formateur présent se garde le droit de ne pas vous répondre ou de vous orienter vers une documentation mais ne restez pas bloqués.
* N'hésitez pas à prendre les tâches dans le désordre quand elles sont indépendantes.
* Terminez les tâches plutôt que démarrer beaucoup de tâches.
* N'hésitez pas à faire des choses dans le back qui ne sont pas accessibles depuis le front : vous montrerez comment ça marche avec Insomnia/Postman et ça sera déjà bien !

### Soutenance

Le dernier jour, vous présenterez votre application devant 4 développeurs. Le format de la soutenance est libre, l'objectif étant de mettre en valeur au mieux le travail fourni. L'idéal est de montrer l'application finale en fonctionnement (via le front ou l'API Rest) mais vous pouvez également montrer :

 - Un diagramme de classes.
 - Un modèle conceptuel de données.
 - Le script SQL de création de la base de données.
 - Une partie du code de votre application qui vous semble particulièrement intéressant ou qui a demandé beaucoup d'effort.
 - ...

Pensez également à montrer si ça vous semble pertinent :

- La pipeline d'intégration continue 
- La façon dont vous construisez et démarrez votre application (avec maven, avec docker...).
- Le résultat des tests unitaires.

## Sujet: Zira

La Zenika Academy souhaite développer un outil pour gérer le développement d'une application sous forme de ticket (User Story). Cet outil est une sorte de Todo liste améliorée.

Il est recommandé de lire l'intégralité de ce document avant de démarrer les développements.

## Description générale de l'application

L'outil présentera des fonctionnalités similaires à la fonctionnalités des issues sur GitHub/GitLab ou encore à la gestion de ticket sur un outil comme Jira.

L'application devra permettre à des utilisateurs de créer des projets. Puis pour chaque projet créer des tickets. Ces projets et tickets sont ensuite disponibles pour tous les visiteurs, même s'ils n'ont pas de compte utilisateur. 

Les tickets peuvent avoir plusieurs états qui seront fixes : TO DO (travail à réaliser), IN PROGRESS (travail en cours), DONE (travail terminé).

Cela permettra un affichage sous forme de tableau Kanban permettant de visualiser l'avancement des développements au sein d'un projet.

## Cahier des charges fonctionnel

Dans ce cahier des charges, on parle de plusieurs catégories de personnes : 
 - Un « Visiteur » est une personne anonyme qui consulte l'outil uniquement en lecture, il ne peut créer/modifier/supprimer des projets ou des tickets.
 - Un « Utilisateur » est une personne qui peut créer/modifier/supprimer des projets et des tickets. Il pourra à terme être authentifié par son pseudonyme et son adresse mail.
 
 Pour simplifier les phrases, on considère qu'un utilisateur est aussi un visiteur (il peut faire tout ce que peut faire un visiteur).

 > La partie « Utilisateur » est un chantier a lui seul, vous pouvez dans un premier temps permettre au visiteur de disposer de tous les droits.

### Chantier « Création des projets/tickets et affichage »

Un projet contient les données suivantes : 
 * Un nom
 * Un descriptif
 * Une date de création
Le descriptif est limité à 500 caractères.

Un ticket rattaché à un projet contient les données suivantes : 
 * Un titre
Le titre ne doit pas dépasser 50 caractères.
 * Un contenu
Le contenu d'un ticket ne doit pas dépasser les 1 000 caractères.
 * Une date de création
 * Un statut (TODO, INPROGRESS, DONE)
Par défaut lors de la création le statut est TODO.

#### Fonctionnalités principales

* Un utilisateur peut créer un projet. 
* Un utilisateur peut créer des tickets dans un projet.
* Un utilisateur peut éditer un projet et un ticket. L'édition d'un ticket doit permettre de modifier les informations du ticket et son état.
* Un visiteur peut afficher la liste des projets (triée par ordre chronologique).
* Un visiteur peut afficher le détail d'un projet et la liste des tickets associée (triée par ordre chronologique et en fonction de leur statut TODO > INPROGRESS > DONE).
* Un visiteur peut afficher le détail d'un ticket.

#### Fonctionnalités « Nice to have »

* Un utilisateur peut supprimer un projet et supprimer un ticket (la suppression du projet doit entraîner la suppression des tickets du projet).
* L'affichage de la liste des tickets est réalisé sous forme d'un tableau de 3 colonnes selon le statut de chaque ticket comme un board Kanban.
* Les tickets sont présentés sous forme de carte et il est possible de faire du drag-and-drop d'une colonne à l'autre pour changer le statut.
* Un pourcentage d'avancement est indiqué au niveau de la vue projet basé sur le nombre de tickets réalisés/nombre total de ticket * 100. Si il n'y a aucun ticket l'avancement est de 0%.
 
### Chantier « Authentification des utilisateurs »

On souhaite enregistrer les informations sur les auteurs des projets et des tickets, ainsi que gérer quelques permissions.

Lorsqu'un projet est créé, on enregistre l'auteur pour référence. L'auteur d'un projet et d'un ticket ne peut pas être modifié. 

Un utilisateur est représente par les données suivantes : 
 * Un nom
 * Un prénom
 * Une adresse email

#### Fonctionnalités principales

* Avoir des utilisateurs enregistrés
  Il n'est pas nécessaire de gérer l'enregistrement d'un nouvel utilisateur. Les utilisateurs peuvent être gérés InMemory.
* Un visiteur peut se connecter sur l'application et devenir un utilisateur
* Mémoriser l'auteur d'un projet et d'un ticket. Les projets et les tickets doivent nécessairement disposer d'un auteur qui peut être dans une première version une simple chaine de caractère avec identifiant (username).
* Seul un utilisateur peut créer un projet et un ticket. Le visiteur est en lecture seule.

#### Fonctionnalités « nice to have »

* Les utilisateurs sont stockés en base de données plutôt qu'InMemory
* Un visiteur peut s'enregistrer sur l'application, se connecter et devenir un utilisateur
* L'édition/suppression d'un projet est restreinte à l'auteur du projet.
* L'édition/suppression d'un ticket est restreinte à l'auteur du ticket.

 
### Chantier « Recherche et Navigation »

L'objectif à moyen terme est d'héberger un grand nombre de projets sur la plateforme. En conséquence, on souhaite fournir un système de recherche des projets.

#### Fonctionnalités principales

* Un visiteur peut rechercher un projet via son titre. La recherche doit trouver les projets dont le titre peut correspondre que partiellement.
* Un utilisateur peut filtrer la vue projet pour ne voir que les projets dont il est l'auteur. Ce filtre se cumule avec la recherche par titre.
* Un visiteur peut cliquer sur le nom d'un auteur au niveau du projet ou au niveau d'un ticket pour afficher une vue détaillé de l'utilisateur avec la liste des projets créés par cet utilisateur et la liste des tickets créés également par cet utilisateur.
 
#### Fonctionnalités « Nice to have »

* Les projets sont affichées sur plusieurs pages numérotées [Spring Data PagingAndSortingRepository doc reference](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.core-concepts) avec 10 projets par pages.
* Un filtre supplémentaire permet de rechercher par Auteur en proposant un champs avec nom et prénom comme dans cet article : [Autocomplete with multiple input fields](https://betterprogramming.pub/angular-material-tricks-autocomplete-with-multiple-input-fields-adebbaae5603)
