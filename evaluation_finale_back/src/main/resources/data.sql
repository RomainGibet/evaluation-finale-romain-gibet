INSERT INTO app_user (name, mail, role)
VALUES ('Robert', 'pouet@gmail.com', 'user')
ON CONFLICT DO NOTHING;

INSERT INTO project (name, description)
VALUES ('Projet X', 'Confidentiel')
ON CONFLICT DO NOTHING;


INSERT INTO ticket (title, content, status, project_id)
VALUES ('Créer une sécurité impénétrable', 'Système de défense à la pointe de la technologie', 'IN PROGRESS', 1)
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, status, project_id)
VALUES ('Fonctionnalité de destruction du site', 'Mise en place d une librairie d autodestruction', 'DONE', 1)
ON CONFLICT DO NOTHING;

INSERT INTO project (name, description)
VALUES ('Pet Clinic', 'Trouvez une clinique Vétérinaire')
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, status, project_id)
VALUES ('Trouver le vétérinaire le plus proche', 'Un user doit pouvoir trouver une clinique vétérinaire', 'IN PROGRESS', 2)
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, status, project_id)
VALUES ('Contacter un vétérinaire', 'Un user doit pouvoir contacter par message un vétérinaire', 'TODO', 2)
ON CONFLICT DO NOTHING;


INSERT INTO app_user (name, mail, role)
VALUES ('Gildas', 'gilou@gmail.com', 'user')
ON CONFLICT DO NOTHING;

INSERT INTO project (name, description)
VALUES ('Zira', 'Management de projet')
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, status, project_id)
VALUES ('Authentification', 'Créer un système d authentification', 'TODO', 3)
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, status, project_id)
VALUES ('Recherche par nom de projet', 'Mettre en place un système de recherche par nom de projet', 'TODO', 3)
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, status, project_id)
VALUES ('Créer un nouveau projet', 'Un utilisateur doit pouvoir créer un nouveau projet et être redirigé sur la page d accueil du site', 'DONE', 3)
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, status, project_id)
VALUES ('Afficher la liste des projets', 'Un visiteur doit pouvoir accèder à la liste des projets', 'DONE', 3)
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, status, project_id)
VALUES ('Créer une modération des routes', 'Les routes doivent être accessible, ou non en fonction du status utilisateur', 'IN PROGRESS', 3)
ON CONFLICT DO NOTHING;





