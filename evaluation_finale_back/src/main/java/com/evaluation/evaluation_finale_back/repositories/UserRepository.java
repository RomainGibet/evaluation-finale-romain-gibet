package com.evaluation.evaluation_finale_back.repositories;

import com.evaluation.evaluation_finale_back.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
