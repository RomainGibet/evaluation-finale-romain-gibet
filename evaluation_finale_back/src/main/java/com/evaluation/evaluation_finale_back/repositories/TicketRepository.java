package com.evaluation.evaluation_finale_back.repositories;

import com.evaluation.evaluation_finale_back.models.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {
}
