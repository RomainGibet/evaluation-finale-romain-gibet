package com.evaluation.evaluation_finale_back.repositories;

import com.evaluation.evaluation_finale_back.models.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Integer> {

    List<Project> findByOrderByCreatedAtDesc();
}