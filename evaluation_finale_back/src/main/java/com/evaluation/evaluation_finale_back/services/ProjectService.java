package com.evaluation.evaluation_finale_back.services;

import com.evaluation.evaluation_finale_back.models.Project;
import com.evaluation.evaluation_finale_back.repositories.ProjectRepository;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {

    ProjectRepository projectRepository;

            public ProjectService(ProjectRepository projectRepository){

                this.projectRepository = projectRepository;

            }


    /**
     * Méthode permettant à un utilisateur de créer un nouveau projet
     *
     * @param project
     */

    public void createProject(Project project) {

        projectRepository.save(project);
    }

    /**
     * Méthode permettant de supprimer un projet
     *
     * @param id
     * @throws Exception
     */

    public void deleteProject(Integer id) throws Exception {

        Optional<Project> optProject = projectRepository.findById(id);

        if(optProject.isPresent()) {

            Project currentProject = optProject.get();
            projectRepository.delete(currentProject);
        }
        else {
            throw new Exception("Request not found");
        }


    }

    /**
     * Méthode permettant de mettre à jour un projet existant
     *
     * @param projectId
     * @param update
     * @throws Exception
     */


    public Project updateProject(Integer projectId, Project update) throws Exception{

        Optional<Project> optProject = projectRepository.findById(projectId);

        if(optProject.isPresent()) {
            Project currentProject = optProject.get();

            currentProject.setId(update.getId());
            currentProject.setDescription(update.getDescription());
            currentProject.setName(update.getName());
//            currentProject.setUser(update.getUser());
            currentProject.setTickets(update.getTickets());

            return projectRepository.save(currentProject);

        } else {
            throw new Exception("Request not found");
        }

    }

    /**
     * Méthode permettant à un utilisateur de récupérer la liste de tous les projets
     * trié par ordre chronologique de création
     * @return
     */
    public List<Project> getAllProjects() {

        return projectRepository.findByOrderByCreatedAtDesc();
    }

    /**
     * Méthode permettant la récupération d'un projet en fonction de son Id
     * Récupération d'un type Optional et vérification de celui-ci
     *
     * @param id
     * @return
     * @throws Exception
     */
    public Project getProjectById(Integer id) throws Exception {

        Optional<Project> projectById = projectRepository.findById(id);
        if(projectById.isPresent()) {
            return projectById.get();
        } else {
             throw new Exception("Request not found");
        }

    }

}
