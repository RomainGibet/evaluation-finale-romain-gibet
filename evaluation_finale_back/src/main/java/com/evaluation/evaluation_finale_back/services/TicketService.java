package com.evaluation.evaluation_finale_back.services;

import com.evaluation.evaluation_finale_back.models.Project;
import com.evaluation.evaluation_finale_back.models.Ticket;
import com.evaluation.evaluation_finale_back.repositories.ProjectRepository;
import com.evaluation.evaluation_finale_back.repositories.TicketRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;


@Service
public class TicketService {
    TicketRepository ticketRepository;
    ProjectRepository projectRepository;

    public TicketService(TicketRepository ticketRepository, ProjectRepository projectRepository) {

        this.ticketRepository = ticketRepository;
        this.projectRepository = projectRepository;

    }

    /**
     * Méthode permettant la création d'un ticket dans un projet
     *
     * @param ticket
     * @param projectId
     */

    public void createTicket(Ticket ticket, Integer projectId){

        ticketRepository.save(ticket);

    }

    /**
     * Méthode permettant de supprimer un ticket
     *
     * @param id
     * @throws Exception
     */

    public void deleteTicket(Integer id) throws Exception {

        Optional<Ticket> optTicket = ticketRepository.findById(id);

        if(optTicket.isPresent()) {

            Ticket currentTicket = optTicket.get();
            ticketRepository.delete(currentTicket);
        }
        else {
            throw new Exception("Request not found");
        }


    }

    /**
     * Méthode permettant la récupération d'un ticket en fonction de son Id
     *
     * @param id
     * @return
     * @throws Exception
     */
    public Ticket getTicketById(Integer id) throws Exception {

        Optional<Ticket> ticketById = ticketRepository.findById(id);
        if(ticketById.isPresent()) {
            return ticketById.get();
        } else {
            throw new Exception("Request not found");
        }

    }


    /**
     * Méthode permettant de mettre à jour un ticket existant
     *
     * @param ticketId
     * @param update
     * @throws Exception
     */


    public Ticket updateTicket(Integer ticketId, Ticket update) throws Exception{

        Optional<Ticket> OptTicket = ticketRepository.findById(ticketId);

        if(OptTicket.isPresent()) {
            Ticket currentTicket = OptTicket.get();

            currentTicket.setTitle(update.getTitle());
            currentTicket.setStatus(update.getStatus());
            currentTicket.setProjectId(update.getProjectId());
            currentTicket.setContent(update.getContent());

            return ticketRepository.save(currentTicket);

        } else {
            throw new Exception("Request not found");
        }


    }



}
