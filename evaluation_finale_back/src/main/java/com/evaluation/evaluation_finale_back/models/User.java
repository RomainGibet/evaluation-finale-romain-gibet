package com.evaluation.evaluation_finale_back.models;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Table(name="app_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="name", nullable = false)
    private String name;

    @Column(name="mail", nullable = false)
    private String mail;

    @Column(name="role", nullable = false)
    private String role;

    @Column(name="created_at")
    private ZonedDateTime created_at;

//    @OneToMany(mappedBy="user", cascade = CascadeType.REMOVE, orphanRemoval = true)
//    @JsonManagedReference
//    private List<Project> projects;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public ZonedDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(ZonedDateTime created_at) {
        this.created_at = created_at;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role= role;
    }

//    public List<Project> getProjects() {
//        return projects;
//    }
//
//    public void setProjects(List<Project> projects) {
//        this.projects = projects;
//    }
}

