package com.evaluation.evaluation_finale_back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication (exclude = SecurityAutoConfiguration.class)
public class EvaluationFinaleBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaluationFinaleBackApplication.class, args);
	}

}

