package com.evaluation.evaluation_finale_back.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;


import java.util.List;

//Configuration
public class SecurityConfiguration {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        // The strictest session creation option – “stateless” – is a guarantee that the application will not create any session at all.
        // No COOKIE !!!!
//        http.sessionManagement(httpSecuritySessionManagementConfigurer -> httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
//
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeHttpRequests()

                .requestMatchers(HttpMethod.GET, "/api/**").permitAll()
                .requestMatchers(HttpMethod.POST, "/api/**").hasRole("USER")
                .requestMatchers(HttpMethod.PUT, "/api/**").hasRole("USER")
                .requestMatchers(HttpMethod.DELETE, "/api/**").hasRole("USER")
                .anyRequest().authenticated();


        http.csrf().disable();

        // Activate httpBasic Authentication https://en.wikipedia.org/wiki/Basic_access_authentication
        // Without Basic realm header in order to avoid internet browser's popup
        //http.httpBasic(httpSecurityHttpBasicConfigurer -> httpSecurityHttpBasicConfigurer.authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED)));

        http.httpBasic();

        return http.build();
    }

        @Bean
        public UserDetailsService userDetailsService() {
            UserDetails lebowski = User.withUsername("lebowski")
                    // 06031998
                    .password("{sha256}f9da241f2c529da8635e2a50751da4adc1bb574a0db8ddf53e04d02778323749128b47d9c1e2f63f")
                    .roles("USER")
                    .build();

            UserDetails abitbol = User.withUsername("abitbol")
                    // 26051921
                    .password("{bcrypt}$2a$10$5Ly8c0ZTrtna.C/aQDYZgeUUzHRMz6rgstocS5xx35fFHn7qx8t9u")
                    .roles("USER")
                    .build();

            UserDetails chuck = User.withUsername("chuck")
                    .password("{noop}10031940")
                    .roles("MANAGER")
                    .build();

            List<UserDetails> users = List.of(lebowski, abitbol, chuck);

            return new InMemoryUserDetailsManager(users);
    }
}
