package com.evaluation.evaluation_finale_back.controllers;

import com.evaluation.evaluation_finale_back.models.Project;
import com.evaluation.evaluation_finale_back.models.Ticket;
import com.evaluation.evaluation_finale_back.repositories.ProjectRepository;
import com.evaluation.evaluation_finale_back.repositories.TicketRepository;
import com.evaluation.evaluation_finale_back.services.ProjectService;
import com.evaluation.evaluation_finale_back.services.TicketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/ticket")

public class TicketController {

    private static final Logger LOG = LoggerFactory.getLogger(TicketController.class);

    private final TicketService ticketService;


    public TicketController(TicketService ticketService) {

        this.ticketService = ticketService;


    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getTicketById(@PathVariable Integer id) throws Exception {

        try {
            Ticket ticket= ticketService.getTicketById(id);
            LOG.info("Ticket récupéré via son id");

            return ResponseEntity.status(HttpStatus.OK).body(ticket);

        } catch (Exception e) {

            LOG.error("Mauvaise requete : " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());

        }
    }

    @RequestMapping(value="/project/{projectId}", method = RequestMethod.POST)
    public ResponseEntity<?> createTicket(@RequestBody Ticket ticket, @PathVariable Integer projectId) throws Exception {

        try {
            ticketService.createTicket(ticket, projectId);
            LOG.info("Ticket créé");
            return ResponseEntity.status(HttpStatus.OK).build();

        } catch (Exception e) {

            LOG.error("Mauvaise requete : " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());

        }
    }

    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTicket(@PathVariable Integer id) throws Exception {

        try {
            ticketService.deleteTicket(id);

            LOG.info("Ticket supprimé");

            return ResponseEntity.status(HttpStatus.OK).build();

        } catch (Exception e) {

            LOG.error("Mauvaise requete : " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());

        }
    }

    @RequestMapping(value="/{ticketId}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateTicket(@RequestBody Ticket ticket, @PathVariable Integer ticketId) throws Exception {

        try {
            ticketService.updateTicket(ticketId, ticket);
            LOG.info("Ticket mis à jour");
            return ResponseEntity.status(HttpStatus.OK).build();

        } catch (Exception e) {

            LOG.error("Mauvaise requete : " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());

        }
    }

}
