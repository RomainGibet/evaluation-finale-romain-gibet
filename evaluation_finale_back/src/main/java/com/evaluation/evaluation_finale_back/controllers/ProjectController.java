package com.evaluation.evaluation_finale_back.controllers;

import com.evaluation.evaluation_finale_back.models.Project;
import com.evaluation.evaluation_finale_back.repositories.ProjectRepository;
import com.evaluation.evaluation_finale_back.services.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/project")
public class ProjectController {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectController.class);

    private final ProjectService projectService;
    private final ProjectRepository projectRepository;


    public ProjectController(ProjectService projectService, ProjectRepository projectRepository ){

        this.projectService = projectService;
        this.projectRepository = projectRepository;

    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getProjects() throws Exception {

        try {
            List<Project> projectsList = projectService.getAllProjects();

            LOG.info("Projet récupéré");
            return ResponseEntity.status(HttpStatus.OK).body(projectsList);



        } catch (Exception e) {

            LOG.error("Mauvaise requete : " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());

        }
    }

    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProject(@PathVariable Integer id) throws Exception {

        try {
        projectService.deleteProject(id);

        LOG.info("Projet supprimé");

        return ResponseEntity.status(HttpStatus.OK).build();

    } catch (Exception e) {

        LOG.error("Mauvaise requete : " + e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());

    }


    }



    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProjectById(@PathVariable Integer id) throws Exception {

        try {
            Project project = projectService.getProjectById(id);

            LOG.info("Projet récupéré via son id");
            return ResponseEntity.status(HttpStatus.OK).body(project);

        } catch (Exception e) {

            LOG.error("Mauvaise requete : " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());

        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createProject(@RequestBody Project project) throws Exception {

        try {
            projectService.createProject(project);
            LOG.info("Projet créé avec succès");
            return ResponseEntity.status(HttpStatus.OK).build();

        } catch (Exception e) {

            LOG.error("Mauvaise requete : " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());

        }
    }

    @RequestMapping(value="/{projectId}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProject(@RequestBody Project project, @PathVariable Integer projectId) throws Exception {

        try {
            projectService.updateProject(projectId, project);

            return ResponseEntity.status(HttpStatus.OK).build();

        } catch (Exception e) {

            LOG.error("Mauvaise requete : " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());

        }
    }

}
