import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Project } from 'src/app/models/project';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private httpClient: HttpClient) { }

  getAllProjects() :Observable<Project[]> {

    return this.httpClient.get<Project[]>('http://localhost:4200/api/project')
  }

}
