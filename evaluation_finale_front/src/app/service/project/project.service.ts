import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Project } from 'src/app/models/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private httpClient :HttpClient) { }
  

  getProjectById(id :number) :Observable<Project> {

    return this.httpClient.get<Project>(`/api/project/${id}`)
  }

  deleteProject(id :number | undefined) :Observable<Project> {

    return this.httpClient.delete<Project>(`/api/project/${id}`)

  }
  
  saveNewProject(project :Project) :Observable<Project | undefined> {

    return this.httpClient.post<Project>(`/api/project`, project)

  }

  updateProject(project :Partial<Project>, id :number) :Observable<Project | undefined> {

    return this.httpClient.put<Project>(`/api/project/${id}`, project)

  }



}
