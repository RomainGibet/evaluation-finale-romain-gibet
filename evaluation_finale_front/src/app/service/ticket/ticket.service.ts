import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ticket } from 'src/app/models/ticket';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private httpClient :HttpClient) { }


  saveNewTicket(id : number | undefined, ticket :Ticket) :Observable<Ticket | undefined> {

    return this.httpClient.post<Ticket>(`/api/ticket/project/${id}`, ticket)

  }

  deleteTicket(id :number | undefined) :Observable<Ticket> {

    return this.httpClient.delete<Ticket>(`/api/ticket/${id}`)

  }

  getTicketById(id :number | undefined) {

    return this.httpClient.get<Ticket>(`/api/ticket/${id}`)
  }

  updateTicket(ticket :Partial<Ticket>, id :number | undefined) :Observable<Ticket| undefined> {

    return this.httpClient.put<Ticket>(`/api/ticket/${id}`, ticket)

  }

}
