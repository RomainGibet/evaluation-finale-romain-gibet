import { Project } from "./project"

export interface Ticket {

    id: number
    title: string
    content: string
    created_at: Date
    status: string
    projectId: number

}