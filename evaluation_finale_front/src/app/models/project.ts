import { Ticket } from "./ticket"

export interface Project {

    id: number
    name : string | null
    description: string | null
    createdAt: string
    tickets: Ticket[]
    
}
