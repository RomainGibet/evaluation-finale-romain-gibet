import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './component/menu/menu.component';
import { HomeComponent } from './pages/home/home.component';
import { DetailProjectComponent } from './pages/detail-project/detail-project.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AddProjectFormComponent } from './pages/add-project-form/add-project-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddTicketFormComponent } from './pages/add-ticket-form/add-ticket-form.component';
import { UpdateProjectFormComponent } from './pages/update-project-form/update-project-form.component';
import { UpdateTicketFormComponent } from './pages/update-ticket-form/update-ticket-form.component';
import { LoginModule } from './pages/login/login.module';
import { BasicAuthInterceptor } from './helpers/basic-auth.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';




@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    DetailProjectComponent,
    AddProjectFormComponent,
    AddTicketFormComponent,
    UpdateProjectFormComponent,
    UpdateTicketFormComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    LoginModule

  ],
  providers: [{
    provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor,
    multi: true
},
{
    provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor,
    multi: true
}],
  bootstrap: [AppComponent]
  
})
export class AppModule { 
  
}
