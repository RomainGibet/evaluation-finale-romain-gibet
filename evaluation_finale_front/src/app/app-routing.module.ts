import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { DetailProjectComponent } from './pages/detail-project/detail-project.component';
import { AddProjectFormComponent } from './pages/add-project-form/add-project-form.component';
import { AddTicketFormComponent } from './pages/add-ticket-form/add-ticket-form.component';
import { UpdateProjectFormComponent } from './pages/update-project-form/update-project-form.component';
import { UpdateTicketFormComponent } from './pages/update-ticket-form/update-ticket-form.component';



const routes: Routes = [

  {path: 'home', component: HomeComponent},

  {path: 'project/new', component: AddProjectFormComponent},
  {path: 'project/:id', component: UpdateProjectFormComponent},

  {path: 'detail_project/:id', component: DetailProjectComponent},
  
 
  {path: 'detail_project/:id/ticket', component: AddTicketFormComponent},
  {path: 'detail_project/:id/ticket/:ticketId', component: UpdateTicketFormComponent},
 

  
  {path: '',   redirectTo: '/home', pathMatch: 'full'},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
