import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Project } from 'src/app/models/project';
import { Ticket } from 'src/app/models/ticket';
import { ProjectService } from 'src/app/service/project/project.service';
import { TicketService } from 'src/app/service/ticket/ticket.service';

@Component({
  selector: 'app-add-ticket-form',
  templateUrl: './add-ticket-form.component.html',
  styleUrls: ['./add-ticket-form.component.css']
})
export class AddTicketFormComponent implements OnInit {

  projectId: number = 0
  currentProjectId : number | undefined
 
  constructor (private ticketService :TicketService, private router: Router, private route : ActivatedRoute, private projectService :ProjectService) {}
  

  addTicketForm = new FormGroup({

    title: new FormControl('', [Validators.required]),
    content: new FormControl('', [Validators.required]),
    status: new FormControl('', [Validators.required]),
  
  })

  ngOnInit(): void {

    this.projectId = Number(this.route.snapshot.paramMap.get('id'))

  }

  addTicket() {

    if(this.addTicketForm.valid) {
      
        const values = this.addTicketForm.value
        const result = {...values, projectId: this.projectId} as Ticket


           this.ticketService.saveNewTicket(this.projectId, result).subscribe(() =>{
            const currentUrl = this.router.url;
      
            this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
              this.router.navigate(['/', 'detail_project', this.projectId]);
            });
      
          })
      
  }
}
}
