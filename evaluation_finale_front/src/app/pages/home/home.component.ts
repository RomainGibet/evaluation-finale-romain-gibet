import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { HomeService } from 'src/app/service/home/home.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {

  projectsData: Project[] = []

  constructor(private homeService :HomeService) {}



ngOnInit(): void {

  this.homeService.getAllProjects().subscribe((project) => 
  this.projectsData.push(...project));
}

}