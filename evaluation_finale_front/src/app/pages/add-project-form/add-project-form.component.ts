import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/service/project/project.service';

@Component({
  selector: 'app-add-project-form',
  templateUrl: './add-project-form.component.html',
  styleUrls: ['./add-project-form.component.css']
})
export class AddProjectFormComponent {

constructor(private projectService :ProjectService, private router: Router) {}


addProjectForm = new FormGroup({

  name: new FormControl('', [Validators.required]),
  description: new FormControl('', [Validators.required]),

})


addProject() {

  if(this.addProjectForm.valid) {

    const result = this.addProjectForm.value as Project
    this.projectService.saveNewProject(result).subscribe(() =>{
      const currentUrl = this.router.url;

      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate(['/', 'home']);
      });
    })
  }

  


 }
 
}
