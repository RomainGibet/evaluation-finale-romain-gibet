import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/service/project/project.service';
import { HomeService } from 'src/app/service/home/home.service';
import { TicketService } from 'src/app/service/ticket/ticket.service';

@Component({
  selector: 'app-detail-project',
  templateUrl: './detail-project.component.html',
  styleUrls: ['./detail-project.component.css']
})
export class DetailProjectComponent implements OnInit  {
  
  
  private readonly LOCAL_STORAGE_KEY = 'currentUser'
  localStorage: any;
  user = localStorage.getItem(this.LOCAL_STORAGE_KEY)


  id: number| undefined
  projectDetail: Project | undefined


  constructor(private ticketService :TicketService, private projectService :ProjectService, private ActRoute: ActivatedRoute, private router: Router) {}


  ngOnInit(): void {

    this.id = Number(this.ActRoute.snapshot.paramMap.get('id'))

    this.projectService.getProjectById(this.id).subscribe((project) => 
    this.projectDetail = project);

  
  }

  deleteProject(id :number | undefined) :void {

    this.projectService.deleteProject(id).subscribe(() =>{

      const currentUrl = this.router.url;

      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate(['/', 'home']);

      });

    })
    
    
    // subscribe(project => {

    //   if(project) {
    //     this.router.navigateByUrl(`/home`);
    //   }
    // });

    

  }

  deleteTicket(id :number | undefined) :void {

    this.ticketService.deleteTicket(id).subscribe(() =>{

      const currentUrl = this.router.url;

      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);

      });

    })
  }

}
