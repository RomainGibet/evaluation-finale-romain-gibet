import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ticket } from 'src/app/models/ticket';
import { ProjectService } from 'src/app/service/project/project.service';
import { TicketService } from 'src/app/service/ticket/ticket.service';

@Component({
  selector: 'app-update-ticket-form',
  templateUrl: './update-ticket-form.component.html',
  styleUrls: ['./update-ticket-form.component.css']
})
export class UpdateTicketFormComponent implements OnInit{

  projectId: number | undefined;
  ticketToUpdateId: number | undefined;
  ticket?: Ticket 

  constructor(private ticketService :TicketService, private router: Router, private route : ActivatedRoute, private projectService :ProjectService) {}
  
  updateTicketForm = new FormGroup({

    title: new FormControl('', [Validators.required]),
    content: new FormControl('', [Validators.required]),
    status: new FormControl('', [Validators.required]),
  
  })

  updateTicket() {

    if(this.updateTicketForm) {

      const values = this.updateTicketForm.value
      const result = {...values, projectId: this.projectId} as Ticket

      this.ticketService.updateTicket(result, this.ticketToUpdateId).subscribe(() =>{
        const currentUrl = this.router.url;
  
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate(['/detail_project/', this.projectId]);
        });
  
      })
    }

  }
  


  ngOnInit(): void {
    
    this.projectId = Number(this.route.snapshot.paramMap.get('id'))
    this.ticketToUpdateId = Number(this.route.snapshot.paramMap.get('ticketId'))
    
    this.ticketService.getTicketById(this.ticketToUpdateId).subscribe(ticket => {

      this.ticket = ticket
      console.log(ticket)

        if(ticket) {

          this.updateTicketForm.patchValue({
            
            title: this.ticket.title,
            content: this.ticket.content,
            status: this.ticket.status,
          
          })
      
        }
    })


  }
}
