import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/service/project/project.service';

@Component({
  selector: 'app-update-project-form',
  templateUrl: './update-project-form.component.html',
  styleUrls: ['./update-project-form.component.css']
})
export class UpdateProjectFormComponent implements OnInit{

  projectToUpdateId :number = 0
  project? :Project

  constructor(private projectService :ProjectService, private router: Router, private route: ActivatedRoute) {


  }

  updateProjectForm = new FormGroup({

    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
  
  })



  updateProject() {

    if(this.updateProjectForm) {

      const values = this.updateProjectForm.value
      const result = { ...values, id: this.projectToUpdateId }

      this.projectService.updateProject(result, this.projectToUpdateId).subscribe(() =>{
        const currentUrl = this.router.url;
  
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate(['/']);
        });
  
      })
    }
  }

  ngOnInit(): void {
    
    this.projectToUpdateId = Number(this.route.snapshot.paramMap.get('id'))
    
    this.projectService.getProjectById(this.projectToUpdateId).subscribe(project => {

      this.project = project

        if(project) {

          this.updateProjectForm.patchValue({
            
            name: this.project.name,
            description: this.project.description,
          
          })
      
        }
    })

  }



}
