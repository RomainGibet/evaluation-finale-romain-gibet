import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {AuthenticationService} from "../../service/authentication.service";


@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  returnUrl: Params | string = {};


  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.form.reset();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    this.authenticationService.login({
      username: this.form.controls.username.value,
      password: this.form.controls.password.value
    });
    // Return to previous url
    // this.router.navigate(['/']);

    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
    this.router.navigate(['/']);
    
    });
  }
}


